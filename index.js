import React from 'react';
import './loader.sass';

function Loader( props ) {
  return (
    <div className="loader">
      { props.children }
      <div className="loader-ripple"><div></div><div></div></div>
    </div>
  );
}

export default Loader;
